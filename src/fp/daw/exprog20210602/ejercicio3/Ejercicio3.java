package fp.daw.exprog20210602.ejercicio3;

import java.util.ArrayDeque;
import java.util.Deque;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ejercicio3 ej = new Ejercicio3();
		Deque<Integer> q = new ArrayDeque<Integer>();
		q.add(2);
		q.add(13);
		q.add(-42);
		q.add(21);
		q.add(4);
		q.add(9);
		q.add(14);
		q.add(-3);
		q.add(11);
		q.add(5);
		ej.invertir(4,q);
	}

	private void invertir(int i, Deque<Integer> cola) {
		if (i > 0) {
			if (cola == null)
				throw new IllegalArgumentException();
			if (i > cola.size())
				throw new IllegalArgumentException();
			
			try {
				Deque<Integer> colaFinal = new ArrayDeque<Integer>();
				for (int j = 0; j < i; j++) {
					colaFinal.push(cola.removeFirst());
				}
				
				colaFinal.addAll(cola);
				System.out.println(colaFinal);
			} catch (Exception e) {
				System.out.println(e);
			}
		} else {
			System.out.println("Parametro es cero o negativo, por lo que no se hace nada");
		}
	}

}
