package fp.daw.exprog20210602.ejercicio1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Programa {

	public static void main(String[] args) {
		
		SortedSet<Alumno> alumnos = new TreeSet<Alumno>(new comparadorApellidos()
				.thenComparing(new comparadorNombre()));
		
		Alumno a1 = new Alumno("Juan","Alvarez",LocalDate.of(2001, Month.FEBRUARY, 19),"DAW",1);
		Alumno a2 = new Alumno("Paco","Llano",LocalDate.of(2003, Month.MARCH, 24),"DAW",1);
		Alumno a3 = new Alumno("Ana","Barcia",LocalDate.of(2004, Month.DECEMBER, 5),"DAW",2);
		Alumno a4 = new Alumno("Ana","Zaragoza",LocalDate.of(2005, Month.AUGUST, 15),"DAW",4);
		alumnos.add(a1);
		alumnos.add(a2);
		alumnos.add(a3);
		alumnos.add(a4);
		
		System.out.println(alumnos);
		
		SortedSet<Alumno> alumnos2 = new TreeSet<Alumno>(new comparadorCiclo()
				.thenComparing(new comparadorCurso())
				.thenComparing(new comparadorApellidos())
				.thenComparing(new comparadorNombre())
				);
		
		alumnos2.addAll(alumnos);
		
		System.out.println(alumnos2);
		
		
		
		try {
			FileOutputStream fout = new FileOutputStream("ejer1.txt",true);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(alumnos.toString());
			oos.writeObject(alumnos2.toString());
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		

	}

}
