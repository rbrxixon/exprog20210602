package fp.daw.exprog20210602.ejercicio1;

import java.util.Comparator;

public class comparadorNombre implements Comparator<Alumno> {

	@Override
	public int compare(Alumno o1, Alumno o2) {
		return o1.getNombre().compareToIgnoreCase(o2.getNombre());
	}
}
  
class comparadorApellidos implements Comparator<Alumno> {
	@Override
	public int compare(Alumno o1, Alumno o2) {
        return o1.getApellidos().compareToIgnoreCase(o2.getApellidos());
    }
}

////

class comparadorCiclo implements Comparator<Alumno>
{
	@Override
	public int compare(Alumno o1, Alumno o2) {
        return o1.getCiclo().compareToIgnoreCase(o2.getCiclo());
    }
}

class comparadorCurso implements Comparator<Alumno>
{
	@Override
	public int compare(Alumno o1, Alumno o2) {
		if (o1.getCurso() < o2.getCurso()) {  
	           return 1;  
	       } else if (o1.getCurso() > o2.getCurso()) {  
	           return -1;  
	       } else {  
	          return 0;  
	       }  
    }
	

}


