package fp.daw.exprog20210602.ejercicio4;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Interfaz {
	private int contador = 0;
	private JFrame frame;
	private String label_txt = "";
	JPanel panel;
	JButton button;
	JButton buttonReiniciar;
	JLabel label;
	
	public Interfaz() {
		initialize();
	}
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setTitle("Examen 3º Evaluación - Ejercicio 4");
		frame.setResizable(false);
		
	
		
		panel = new JPanel(new GridLayout(3,0));
		button = new JButton("Pulse Aqui");
		
		buttonReiniciar = new JButton("Reiniciar");
		panel.add(button);
		panel.add(buttonReiniciar);
		
		
		frame.add(panel);
		label = new JLabel();
		label_txt = String.valueOf(contador);
		label.setText(label_txt);

		panel.add(label);
		
		button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contador++;
                System.out.println("Click " + contador);
                label_txt = String.valueOf(contador);
                label.setText(label_txt);
            }
        });
		
		
		frame.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				System.out.println("AQUi");
				System.out.println(e.getKeyChar());
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
            
		
		buttonReiniciar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contador = 0;
                System.out.println("Reiniciar " + contador);
                label_txt = String.valueOf(contador);
                label.setText(label_txt);
            }
        });
	}
}
        

