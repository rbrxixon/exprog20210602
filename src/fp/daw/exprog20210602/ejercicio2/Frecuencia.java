package fp.daw.exprog20210602.ejercicio2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Frecuencia {
	File file;
	Map<String,Integer> datos;
	
	public Frecuencia(File file) {
		super();
		this.file = file;
		this.datos = leerFichero(this.file);
	}
	
	public int comprobarFrecuencia(String palabra) {
		if (datos.get(palabra) == null) {
			System.out.println("Palabra " + palabra + " No encontrada");
			return 0; 
		} else
			return datos.get(palabra);
	}
	
	public Map leerFichero(File f) {
		datos = new HashMap<String, Integer>();
		List<String> listaPalabras = new ArrayList<String>();
		String[] cadena1 = null;
		
		try {
			InputStream is = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
			String str;
		    while ((str = br.readLine()) != null) {
		    	str = str.replace(".", "");
		    	str = str.replace(",", "");
		    	str = str.replace("?", "");
		    	str = str.replace("�", "");
		    	str = str.replace("!", "");
		    	str = str.replace("�", "");
		    	str = str.replace("-", "");
		    	str = str.replace("_", "");
		    	str = str.replace("(", "");
		    	str = str.replace(")", "");
		    	str = str.replace("]", "");
		    	str = str.replace("[", "");
		    	str = str.replace("=", "");
			    	
		    	cadena1 = str.split(" ");
		    	for (int i = 0; i < cadena1.length; i++) {
		    		if (cadena1[i].matches("[a-zA-Z]+")) {
		    			listaPalabras.add(cadena1[i]);
		    		}
				}
		    }

		    br.close();
	 	} catch (Exception e) {
			// TODO: handle exception
	 		System.out.println(e);
		}
		
		for (String palabra : listaPalabras) {
			if (datos.containsKey(palabra)) {
	    		datos.put(palabra, (datos.get(palabra) + 1) );	
	    	} else {
	    		datos.put(palabra, 1);		
	    	}
		}

		return datos;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Map<String, Integer> getDatos() {
		return datos;
	}

	public void setDatos(Map<String, Integer> datos) {
		this.datos = datos;
	}




	public static void main(String[] args) {
		String fileName = "C:\\Users\\alumno\\git\\exprog20210602\\src\\fp\\daw\\exprog20210602\\ejercicio2\\El Quijote ISO-8859-1.txt";
		File f = new File(fileName);
		Frecuencia fr = new Frecuencia(f);
		//fr.comprobarFrecuencia("esta");
		Scanner sc = new Scanner(System.in);
		String entrada;
		
		boolean seguir = true;
		
		do {
			System.out.println("Escribe palabra para comprobar frecuencia: ");
			try {
				entrada = sc.nextLine();
				System.out.println(fr.comprobarFrecuencia(entrada));
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
		}
		while (seguir);

	}
}

